INSERT INTO category (id, name, path, parent_id, created_at, updated_at) VALUES (1, 'category1', 'path1', null, '1984-11-16 17:10:00.000', '1984-11-16 17:10:00.000');
INSERT INTO category (id, name, path, parent_id, created_at, updated_at) VALUES (2, 'category2', 'path2', null, '1984-11-16 17:10:00.000', '1984-11-16 17:10:00.000');
INSERT INTO category (id, name, path, parent_id, created_at, updated_at) VALUES (3, 'category3', 'path3', 1, '1984-11-16 17:10:00.000', '1984-11-16 17:10:00.000');
INSERT INTO category (id, name, path, parent_id, created_at, updated_at) VALUES (4, 'category4', 'path4', 1, '1984-11-16 17:10:00.000', '1984-11-16 17:10:00.000');
INSERT INTO category (id, name, path, parent_id, created_at, updated_at) VALUES (5, 'category5', 'path5', 3, '1984-11-16 17:10:00.000', '1984-11-16 17:10:00.000');
INSERT INTO category (id, name, path, parent_id, created_at, updated_at) VALUES (6, 'category6', 'path6', 2, '1984-11-16 17:10:00.000', '1984-11-16 17:10:00.000');
INSERT INTO category (id, name, path, parent_id, created_at, updated_at) VALUES (7, 'category7', 'path7', 2, '1984-11-16 17:10:00.000', '1984-11-16 17:10:00.000');
INSERT INTO category (id, name, path, parent_id, created_at, updated_at) VALUES (8, 'category8', 'path8', 2, '1984-11-16 17:10:00.000', '1984-11-16 17:10:00.000');
INSERT INTO category (id, name, path, parent_id, created_at, updated_at) VALUES (9, 'category9', 'path9', 6, '1984-11-16 17:10:00.000', '1984-11-16 17:10:00.000');
INSERT INTO category (id, name, path, parent_id, created_at, updated_at) VALUES (10, 'category10', 'path10', null, '1984-11-16 17:10:00.000', '1984-11-16 17:10:00.000');
INSERT INTO category (id, name, path, parent_id, created_at, updated_at) VALUES (11, 'category11', 'path11', null, '1984-11-16 17:10:00.000', '1984-11-16 17:10:00.000');
INSERT INTO category (id, name, path, parent_id, created_at, updated_at) VALUES (12, 'category12', 'path12', null, '1984-11-16 17:10:00.000', '1984-11-16 17:10:00.000');
INSERT INTO category (id, name, path, parent_id, created_at, updated_at) VALUES (13, 'category13', 'path13', null, '1984-11-16 17:10:00.000', '1984-11-16 17:10:00.000');
INSERT INTO category (id, name, path, parent_id, created_at, updated_at) VALUES (14, 'category14', 'path14', null, '1984-11-16 17:10:00.000', '1984-11-16 17:10:00.000');
INSERT INTO category (id, name, path, parent_id, created_at, updated_at) VALUES (15, 'category15', 'path15', 14, '1984-11-16 17:10:00.000', '1984-11-16 17:10:00.000');

INSERT INTO price (id, price, discount, currency, discount_start, discount_end, created_at, updated_at) VALUES (1, 50.5541, 10, 'EUR', '1984-11-16 17:10:00.000', null, '1984-11-16 17:10:00.000', '1984-11-16 17:10:00.000');
INSERT INTO price (id, price, discount, currency, discount_start, discount_end, created_at, updated_at) VALUES (2, 150, null, 'EUR', null, null, '1984-11-16 17:10:00.000', '1984-11-16 17:10:00.000');
INSERT INTO price (id, price, discount, currency, discount_start, discount_end, created_at, updated_at) VALUES (3, 100, null, 'EUR', null, null, '1984-11-16 17:10:00.000', '1984-11-16 17:10:00.000');
INSERT INTO price (id, price, discount, currency, discount_start, discount_end, created_at, updated_at) VALUES (4, 87.25, null, 'EUR', null, null, '1984-11-16 17:10:00.000', '1984-11-16 17:10:00.000');
INSERT INTO price (id, price, discount, currency, discount_start, discount_end, created_at, updated_at) VALUES (5, 10, null, 'EUR', null, null, '1984-11-16 17:10:00.000', '1984-11-16 17:10:00.000');
INSERT INTO price (id, price, discount, currency, discount_start, discount_end, created_at, updated_at) VALUES (6, 4.5, null, 'EUR', null, null, '1984-11-16 17:10:00.000', '1984-11-16 17:10:00.000');
INSERT INTO price (id, price, discount, currency, discount_start, discount_end, created_at, updated_at) VALUES (7, 12, null, 'EUR', null, null, '1984-11-16 17:10:00.000', '1984-11-16 17:10:00.000');
INSERT INTO price (id, price, discount, currency, discount_start, discount_end, created_at, updated_at) VALUES (8, 18.99, null, 'EUR', null, null, '1984-11-16 17:10:00.000', '1984-11-16 17:10:00.000');
INSERT INTO price (id, price, discount, currency, discount_start, discount_end, created_at, updated_at) VALUES (9, 25.27, 70, 'EUR', '1984-11-16 17:10:00.000', '1991-09-21 13:00:00.000', '1984-11-16 17:10:00.000', '1991-09-20 11:00:00.000');
INSERT INTO price (id, price, discount, currency, discount_start, discount_end, created_at, updated_at) VALUES (10, 18.12, null, 'EUR', null, null, '1984-11-16 17:10:00.000', '1984-11-16 17:10:00.000');
INSERT INTO price (id, price, discount, currency, discount_start, discount_end, created_at, updated_at) VALUES (11, 72.20, null, 'EUR', null, null, '1984-11-16 17:10:00.000', '1984-11-16 17:10:00.000');
INSERT INTO price (id, price, discount, currency, discount_start, discount_end, created_at, updated_at) VALUES (12, 100, null, 'EUR', null, null, '1984-11-16 17:10:00.000', '1984-11-16 17:10:00.000');


INSERT INTO product (id, name, description, price_id, category_id, created_at, updated_at) VALUES (1, 'product1', null, 1, 4, '1984-11-16 17:10:00.000', '1984-11-16 17:10:00.000');
INSERT INTO product (id, name, description, price_id, category_id, created_at, updated_at) VALUES (2, 'product2', null, 2, 1, '1984-11-16 17:10:00.000', '1984-11-16 17:10:00.000');
INSERT INTO product (id, name, description, price_id, category_id, created_at, updated_at) VALUES (3, 'product3', null, 3, 8, '1984-11-16 17:10:00.000', '1984-11-16 17:10:00.000');
INSERT INTO product (id, name, description, price_id, category_id, created_at, updated_at) VALUES (4, 'product4', null, 4, 5, '1984-11-16 17:10:00.000', '1984-11-16 17:10:00.000');
INSERT INTO product (id, name, description, price_id, category_id, created_at, updated_at) VALUES (5, 'product5', null, 5, 6, '1984-11-16 17:10:00.000', '1984-11-16 17:10:00.000');
INSERT INTO product (id, name, description, price_id, category_id, created_at, updated_at) VALUES (6, 'product6', null, 6, 4, '1984-11-16 17:10:00.000', '1984-11-16 17:10:00.000');
INSERT INTO product (id, name, description, price_id, category_id, created_at, updated_at) VALUES (7, 'product7', null, 7, 2, '1984-11-16 17:10:00.000', '1984-11-16 17:10:00.000');
INSERT INTO product (id, name, description, price_id, category_id, created_at, updated_at) VALUES (8, 'product8', null, 8, 3, '1984-11-16 17:10:00.000', '1984-11-16 17:10:00.000');
INSERT INTO product (id, name, description, price_id, category_id, created_at, updated_at) VALUES (9, 'product9', null, 9, 9, '1984-11-16 17:10:00.000', '1984-11-16 17:10:00.000');
INSERT INTO product (id, name, description, price_id, category_id, created_at, updated_at) VALUES (10, 'product10', null, 10, 1, '1984-11-16 17:10:00.000', '1984-11-16 17:10:00.000');
INSERT INTO product (id, name, description, price_id, category_id, created_at, updated_at) VALUES (11, 'product11', null, 11, 11, '1984-11-16 17:10:00.000', '1984-11-16 17:10:00.000');
INSERT INTO product (id, name, description, price_id, category_id, created_at, updated_at) VALUES (12, 'product12', null, 12, 12, '1984-11-16 17:10:00.000', '1984-11-16 17:10:00.000');

