package com.levon.interview.mediamarkt.repository;

import com.levon.interview.mediamarkt.entity.Product;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductRepository extends CrudRepository<Product, Long> {

    long countByCategoryId(long id);
}
