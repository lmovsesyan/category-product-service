package com.levon.interview.mediamarkt.repository;

import com.levon.interview.mediamarkt.entity.Category;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CategoryRepository extends CrudRepository<Category, Long> {

    /**
     * Counts sub-categories
     * <p/>
     * <b>NOTE:</b> Does not include sub-categories!
     */
    long countByParentId(long parentCategoryId);

    /**
     * Counts products count which are under given category.
     * <p/>
     * <b>NOTE:</b> Does not include sub-categories!
     */
    @Query("select count(p) from Product p where p.category.id = :categoryId")
    long countCategoryProducts(@Param("categoryId") long categoryId);

    List<Category> findByParentId(Long parentId);
}
