package com.levon.interview.mediamarkt.util;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;

public class PriceUtil {

    /**
     * Sets the scale of the price to two
     */
    public static BigDecimal roundPrice(BigDecimal price) {
        return price.setScale(2, RoundingMode.HALF_UP);
    }

    /**
     * Calculates discounted price
     *
     * @param price current price
     * @param discount discount in percentage
     * @return discounted price
     */
    public static BigDecimal calculateDiscountPrice(BigDecimal price, int discount) {
        return roundPrice(BigDecimal.valueOf(price.doubleValue() - (price.doubleValue() * discount / 100)));
    }

    /**
     * Checks whether the price is valid or not
     * <p>
     * NOTE: The price is valid if its not null and greater than 0
     */
    public static boolean isValidPrice(BigDecimal price) {
        return price != null && price.compareTo(BigDecimal.ZERO) > 0;
    }

    /**
     * Checks whether the discount percentage is valid number
     * <p>
     * NOTE: The discount percentage is valid if <br/>
     * - 100 > percentage > 0
     */
    public static boolean isValidDiscount(Integer discount) {
        return discount != null && discount > 0 && discount < 100;
    }

    /**
     * Checks whether the discount date range is valid or not
     * <p>
     * TNOTE: he date range is valid if <br/>
     *  - both dates are not provided (both are null)<br/>
     *  - or only one is provided (only one is null)<br/>
     *  - or both dates are provided and start date is before end date
     */
    public static boolean isValidDiscountDataRange(Date start, Date end) {
        if ((start == null && end == null) || (start == null || end == null))  {
            return true;
        } else {
            return start.before(end);
        }
    }

    /**
     * Checks whether the discount is valid at this moment
     */
    public static boolean isDiscountExpired(Date discountStart, Date discountEnd) {
        final Date now = new Date();
        return (discountStart != null && now.before(discountStart)) || (discountEnd != null && now.after(discountEnd));
    }
}
