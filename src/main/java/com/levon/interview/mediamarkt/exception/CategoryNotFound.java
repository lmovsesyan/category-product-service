package com.levon.interview.mediamarkt.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class CategoryNotFound extends RuntimeException {

    public CategoryNotFound(Long categoryId)  {
        super(String.valueOf(categoryId));
    }
}
