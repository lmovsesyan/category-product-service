package com.levon.interview.mediamarkt.entity;

import lombok.Data;
import lombok.ToString;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@ToString(exclude = "product")
@Data
@Entity
@Table(name="price", indexes = {@Index(name = "price_idx", columnList = "price")})
public class Price {

    @Id
    @Column(name="id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name="price", precision=12, scale=5, nullable = false)
    private BigDecimal price;

    @Column(name="currency", nullable = false)
    private String currency;

    @Column(name="discount")
    private Integer discount;

    @Column(name="discount_start")
    private Date discountStart;

    @Column(name="discount_end")
    private Date discountEnd;

    @OneToOne(mappedBy = "price", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Product product;

    @Column(name="created_at")
    private Date createdAt;

    @Column(name="updated_at")
    private Date updatedAt;
}
