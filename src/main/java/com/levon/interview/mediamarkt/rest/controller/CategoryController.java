package com.levon.interview.mediamarkt.rest.controller;

import com.levon.interview.mediamarkt.rest.dto.CategoryCreate;
import com.levon.interview.mediamarkt.rest.dto.CategoryDTO;
import com.levon.interview.mediamarkt.rest.dto.CategoryUpdate;
import com.levon.interview.mediamarkt.service.CategoryService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

import static com.levon.interview.mediamarkt.rest.util.Constant.VERSION_1_BASE_PATH;

@RestController
@RequestMapping(value = VERSION_1_BASE_PATH + "category")
public class CategoryController {

    private final CategoryService categoryService;

    @Autowired
    public CategoryController(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    @ApiOperation("Retrieves a category by id")
    @RequestMapping(method = RequestMethod.GET, path = "/{categoryId}")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Category successfully fetched"),
            @ApiResponse(code = 404, message = "Category does not exist")})
    public ResponseEntity<CategoryDTO> category(@PathVariable Long categoryId) {
        final CategoryDTO categoryDTO = categoryService.getCategoryDTO(categoryId);
        return ResponseEntity.ok(categoryDTO);
    }

    @ApiOperation(value = "Retrieves immediate subcategories",
            notes = "We strongly recommend, if it is possible, use lazy loading strategy to get all level subcategories" +
                    "Depending on who is the user of the REST API 'get all level subcategories' API could perform more" +
                    "efficient performance.")
    @RequestMapping(method = RequestMethod.GET, path = "/subcategories/{categoryId}")
    @ApiResponses(value = {
                    @ApiResponse(code = 200, message = "Subcategories successfully fetched"),
                    @ApiResponse(code = 404, message = "Category  does not exist")})
    public ResponseEntity<List<CategoryDTO>> subcategory(@PathVariable Long categoryId) {
        final List<CategoryDTO> categoriesResult = categoryService.getImmediateSubcategories(categoryId);
        return ResponseEntity.ok(categoriesResult);
    }

    @ApiOperation(value = "Retrieves top categories")
    @RequestMapping(method = RequestMethod.GET, path = "/top")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Top categories successfully fetched")})
    public ResponseEntity<List<CategoryDTO>> topCategories() {
        final List<CategoryDTO> topCategories = categoryService.getTopCategories();
        return ResponseEntity.ok(topCategories);
    }

    @ApiOperation("Creates a new category")
    @RequestMapping(method = RequestMethod.POST)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Category has been created")})
    public ResponseEntity<Long> category(@Valid @RequestBody CategoryCreate category) {
        final Long categoryId = categoryService.create(category);
        return ResponseEntity.ok(categoryId);
    }

    @ApiOperation("Updates a category")
    @RequestMapping(method = RequestMethod.PUT)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Category has been updated")})
    public ResponseEntity<Boolean> update(@Valid @RequestBody CategoryUpdate category) {
        categoryService.update(category);
        return ResponseEntity.ok(true);
    }

    @ApiOperation("Deletes a category")
    @RequestMapping(method = RequestMethod.DELETE, path = "/{categoryId}")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Category has been deleted")})
    public ResponseEntity<Boolean> delete(@PathVariable Long categoryId) {
        categoryService.delete(categoryId);
        return ResponseEntity.ok(true);
    }
}
