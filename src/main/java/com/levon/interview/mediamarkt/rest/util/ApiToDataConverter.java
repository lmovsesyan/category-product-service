package com.levon.interview.mediamarkt.rest.util;

import com.levon.interview.mediamarkt.entity.Category;
import com.levon.interview.mediamarkt.entity.Price;
import com.levon.interview.mediamarkt.entity.Product;
import com.levon.interview.mediamarkt.rest.dto.CategoryDTO;
import com.levon.interview.mediamarkt.rest.dto.ProductDTO;
import com.levon.interview.mediamarkt.util.PriceUtil;

import java.math.BigDecimal;

/**
 * Helper class to convert from DBO objects to DTO objects
 */
public class ApiToDataConverter {

    public static CategoryDTO convertToResult(Category category) {
        CategoryDTO categoryDTO = new CategoryDTO();
        categoryDTO.setId(category.getId());
        categoryDTO.setName(category.getName());
        categoryDTO.setPath(category.getPath());
        if (category.getParent() != null) {
            categoryDTO.setParentId(category.getParent().getId());
        }
        return categoryDTO;
    }

    public static ProductDTO convertToResult(Product product) {
        ProductDTO productDTO = new ProductDTO();

        productDTO.setId(product.getId());
        productDTO.setName(product.getName());
        productDTO.setDescription(product.getDescription());

        Price price = product.getPrice();
        final BigDecimal roundedPrice = PriceUtil.roundPrice(price.getPrice());
        if (price.getDiscount() == null) {
            productDTO.setPrice(roundedPrice);
        } else {
            if (PriceUtil.isDiscountExpired(price.getDiscountStart(), price.getDiscountEnd())) {
                productDTO.setPrice(roundedPrice);
            } else {
                productDTO.setDiscount(price.getDiscount());
                productDTO.setPreviousPrice(roundedPrice);
                productDTO.setPrice(PriceUtil.calculateDiscountPrice(price.getPrice(), price.getDiscount()));
            }
        }
        return productDTO;
    }
}
