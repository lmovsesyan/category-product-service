package com.levon.interview.mediamarkt.rest.controller;

import com.levon.interview.mediamarkt.rest.dto.DiscountUpdate;
import com.levon.interview.mediamarkt.rest.dto.ProductCreate;
import com.levon.interview.mediamarkt.rest.dto.ProductDTO;
import com.levon.interview.mediamarkt.rest.dto.ProductUpdate;
import com.levon.interview.mediamarkt.service.ProductService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

import java.util.List;

import static com.levon.interview.mediamarkt.rest.util.Constant.VERSION_1_BASE_PATH;

@RestController
@RequestMapping(value = VERSION_1_BASE_PATH + "product")
public class ProductController {

    private final ProductService productService;

    @Autowired
    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    @ApiOperation(value = "Retrieves a product by id")
    @RequestMapping(method = RequestMethod.GET, path = "/{productId}")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Product successfully fetched"),
            @ApiResponse(code = 404, message = "Product does not exist")})
    public ResponseEntity<ProductDTO> get(@PathVariable Long productId) {
        final ProductDTO productDTO = productService.getProduct(productId);
        return ResponseEntity.ok(productDTO);
    }

    @ApiOperation(value = "Creates a new product",
            notes = "By creating a new product, the product price is important while the currency is optional. " +
                    "In case, if currency is null the default currency will be taken. " +
                    "In case, if other currency is provided, the price will be converted into shop default currency")
    @RequestMapping(method = RequestMethod.POST)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Product has been created")})
    public ResponseEntity<Long> create(@Valid @RequestBody ProductCreate productCreate) {
        Long productId= productService.create(productCreate);
        return ResponseEntity.ok(productId);
    }

    @ApiOperation(value = "Updates a product",
            notes = "By updating an existing product, the product price and currency is optional. " +
                    "In case, if price is provided and the currency is null the default currency will be taken. " +
                    "In case, if price is provided and other currency is provided, the price will be converted into shop default currency")
    @RequestMapping(method = RequestMethod.PUT)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Product has been updated")})
    public ResponseEntity<Boolean> update(@Valid @RequestBody ProductUpdate productUpdate) {
        productService.update(productUpdate);
        return ResponseEntity.ok(true);
    }

    @ApiOperation(value = "Updates a product's discount",
            notes = "In order to update or create discount on product, simply fill the discount percentage field with number between 1-99. " +
                    "Optional field startDate indicates the discount start date. " +
                    "Optional field endDate indicates the discount expiry date. " +
                    "If both dates are missing the discount is always valid")
    @RequestMapping(method = RequestMethod.PUT, path = "/discount")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Product's discount has been updated")})
    public ResponseEntity<Boolean> updateDiscount(@Valid @RequestBody DiscountUpdate discountUpdate) {
        productService.updateDiscount(discountUpdate);
        return ResponseEntity.ok(true);
    }

    @ApiOperation("Deletes a product")
    @RequestMapping(method = RequestMethod.DELETE, path = "/{productId}")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Product has been deleted")})
    public ResponseEntity<Boolean> delete(@PathVariable Long productId) {
        productService.delete(productId);
        return ResponseEntity.ok(true);
    }

    @ApiOperation(value = "Retrieves one level category products")
    @RequestMapping(method = RequestMethod.GET, path = "/category/{categoryId}")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Category products successfully fetched"),
            @ApiResponse(code = 404, message = "Category does not exist")})
    public ResponseEntity<List<ProductDTO>> categoryProducts(@PathVariable Long categoryId) {
        final List<ProductDTO> categoriesResult = productService.getCategoryProducts(categoryId);
        return ResponseEntity.ok(categoriesResult);
    }
}
