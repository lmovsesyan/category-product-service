package com.levon.interview.mediamarkt.rest.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class CategoryUpdate {

    @NotNull
    private Long id;

    private String name;

    private String path;

    private Long parentId;
}