package com.levon.interview.mediamarkt.rest.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;
import java.util.Date;

@Data
public class DiscountUpdate {

    @NotNull
    private final Long productId;

    @PositiveOrZero
    private Integer discount;

    private Date startDate;

    private Date endDate;
}
