package com.levon.interview.mediamarkt.rest.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class CategoryCreate {

    @NotBlank
    private String name;

    @NotBlank
    private String path;

    private Long parentId;
}
