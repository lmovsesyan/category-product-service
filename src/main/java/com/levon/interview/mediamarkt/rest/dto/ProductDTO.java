package com.levon.interview.mediamarkt.rest.dto;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class ProductDTO {

    private long id;

    private String name;

    private String description;

    //private Long categoryId;

    private CategoryDTO category;

    private BigDecimal price;

    private BigDecimal previousPrice;

    private Integer discount;
}
