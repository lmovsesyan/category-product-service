package com.levon.interview.mediamarkt.rest.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.math.BigDecimal;

@Data
public class ProductCreate {

    @NotBlank
    private String name;

    private String description;

    private Long categoryId;

    private String currency;

    @NotNull
    @Positive
    private BigDecimal price;
}
