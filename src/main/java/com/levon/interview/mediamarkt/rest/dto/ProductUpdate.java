package com.levon.interview.mediamarkt.rest.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Data
public class ProductUpdate {

    @NotNull
    private Long id;

    private String name;

    private String description;

    private Long categoryId;

    private BigDecimal price;
    private String currency;
}
