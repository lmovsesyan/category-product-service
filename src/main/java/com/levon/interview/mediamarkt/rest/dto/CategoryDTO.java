package com.levon.interview.mediamarkt.rest.dto;

import lombok.Data;

@Data
public class CategoryDTO {

    private long id;

    private String name;

    private String path;

    private String fullPath;

    private Long parentId;
}
