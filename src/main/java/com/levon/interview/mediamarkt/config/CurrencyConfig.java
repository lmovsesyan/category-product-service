package com.levon.interview.mediamarkt.config;

import com.levon.interview.mediamarkt.client.currency.CurrencyExchangeClient;
import com.levon.interview.mediamarkt.client.currency.FixerConverterClient;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.BufferingClientHttpRequestFactory;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

@Configuration
public class CurrencyConfig {

    @Getter
    @Value("${price.currency.default}")
    private String defaultCurrency;

    @Value("${price.currency.api.fixer.url}")
    private String fixerUrl;

    @Getter
    @Value("${price.currency.api.timeout.read}")
    private Integer readTimeout;

    @Getter
    @Value("${price.currency.api.timeout.connection}")
    private Integer connectionTimeout;

    private RestTemplate restTemplate() {
        SimpleClientHttpRequestFactory factory = new SimpleClientHttpRequestFactory();
        factory.setReadTimeout(readTimeout);
        factory.setConnectTimeout(connectionTimeout);
        return new RestTemplate(new BufferingClientHttpRequestFactory(factory));
    }

    @Bean(name = "fixer")
    public FixerConverterClient currencyExchangeClient() {
        return new FixerConverterClient(restTemplate(), defaultCurrency, fixerUrl);
    }
}
