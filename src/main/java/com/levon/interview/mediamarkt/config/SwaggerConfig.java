package com.levon.interview.mediamarkt.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import static com.levon.interview.mediamarkt.Profile.NOT_TEST;
import static com.levon.interview.mediamarkt.rest.util.Constant.VERSION_1_BASE_PATH;

@Configuration
@EnableSwagger2
@Profile({NOT_TEST})
public class SwaggerConfig {

    private static ApiInfo apiInfo;

    @Value("${info.app.owner}")
    private String owner;

    @Value("${info.app.email}")
    private String email;

    @Value("${info.app.version}")
    private String version;

    @Value("${info.app.name}")
    private String name;

    @Value("${info.app.description}")
    private String description;

    @Bean
    public Docket category() {
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("Category")
                .apiInfo(getInfo())
                .select()
                .paths(PathSelectors.ant(VERSION_1_BASE_PATH + "category/**"))
                .build();
    }

    @Bean
    public Docket product() {
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("Product")
                .apiInfo(getInfo())
                .select()
                .paths(PathSelectors.ant(VERSION_1_BASE_PATH + "product/**"))
                .build();
    }

    private ApiInfo getInfo() {
        if (apiInfo == null) {
            apiInfo = new ApiInfo(name, description,
                    version, "",
                    new Contact(owner, "", email),
                    "", "");
        }
        return apiInfo;
    }
}
