package com.levon.interview.mediamarkt.service;

import com.levon.interview.mediamarkt.client.currency.CurrencyExchangeClient;
import com.levon.interview.mediamarkt.config.CurrencyConfig;
import com.levon.interview.mediamarkt.entity.Price;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Date;

@Service
public class PriceService {

    private final CurrencyConfig currencyConfig;
    private final CurrencyExchangeClient currencyExchangeClient;

    @Autowired
    public PriceService(CurrencyConfig currencyConfig,
                        @Qualifier("fixer") CurrencyExchangeClient currencyExchangeClient) {
        this.currencyConfig = currencyConfig;
        this.currencyExchangeClient = currencyExchangeClient;
    }

    /**
     * Converts currency and builds the price DBO
     */
    public Price buildPrice(BigDecimal price, String currency) {
        Price priceDBO = new Price();

        currency = getOrDefaultCurrency(currency);

        final BigDecimal convertedPrice = convertPrice(price, currency);

        priceDBO.setCurrency(currency);
        priceDBO.setPrice(convertedPrice);
        final Date now = new Date();
        priceDBO.setCreatedAt(now);
        priceDBO.setUpdatedAt(now);

        return priceDBO;
    }

    /**
     * Converts currency and returns the converted price
     */
    public BigDecimal convertPrice(BigDecimal price, String currency) {
        currency = getOrDefaultCurrency(currency);

        final BigDecimal convertedPrice;
        if (StringUtils.equals(currency, currencyConfig.getDefaultCurrency())) {
            convertedPrice = price;
        } else {
            convertedPrice = currencyExchangeClient.convertAmount(price, currency);
        }

        return convertedPrice;
    }

    /**
     * Returns the default currency if the given one is blank, otherwise returns back the same
     */
    private String getOrDefaultCurrency(String currency) {
        return StringUtils.isBlank(currency) ? currencyConfig.getDefaultCurrency() : currency;
    }
}
