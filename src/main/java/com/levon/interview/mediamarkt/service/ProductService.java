package com.levon.interview.mediamarkt.service;

import com.levon.interview.mediamarkt.entity.Category;
import com.levon.interview.mediamarkt.entity.Price;
import com.levon.interview.mediamarkt.entity.Product;
import com.levon.interview.mediamarkt.exception.CategoryNotFound;
import com.levon.interview.mediamarkt.exception.InvalidPriceException;
import com.levon.interview.mediamarkt.exception.ProductNotFound;
import com.levon.interview.mediamarkt.repository.ProductRepository;
import com.levon.interview.mediamarkt.rest.dto.*;
import com.levon.interview.mediamarkt.rest.util.ApiToDataConverter;
import com.levon.interview.mediamarkt.util.PriceUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service
@Transactional
public class ProductService {

    private final PriceService priceService;
    private final CategoryService categoryService;

    private final ProductRepository productRepository;

    @Autowired
    public ProductService(ProductRepository productRepository, CategoryService categoryService,
                          PriceService priceService) {
        this.productRepository = productRepository;
        this.categoryService = categoryService;
        this.priceService = priceService;
    }

    public ProductDTO getProduct(Long productId) {
        final Product product =  productRepository.findOne(productId);
        validate(product, productId);

        log.debug("Product is fetched: {}", product);

        ProductDTO productDTO = ApiToDataConverter.convertToResult(product);
        if (product.getCategory() != null) {
            final Long categoryId = product.getCategory().getId();
            CategoryDTO categoryDTO = categoryService.getCategoryDTO(categoryId);
            productDTO.setCategory(categoryDTO);
        }

        log.debug("Product is prepared: {}", productDTO);
        return productDTO;
    }

    public long create(ProductCreate productCreate) {
        Product product = new Product();
        product.setName(productCreate.getName());
        product.setDescription(productCreate.getDescription());

        setCategory(product, productCreate);

        setPrice(product, productCreate);

        final Date now = new Date();
        product.setCreatedAt(now);
        product.setUpdatedAt(now);
        product = productRepository.save(product);

        log.info("Product is successfully created: {}", product);
        return product.getId();
    }

    public void update(ProductUpdate productUpdate) {
        final Long productId = productUpdate.getId();
        final Product product = productRepository.findOne(productId);
        validate(product, productId);

        updateCategory(product, productUpdate);

        updatePrice(product, productUpdate);

        if (StringUtils.isNotBlank(productUpdate.getName())) {
            product.setName(productUpdate.getName());
        }
        if (StringUtils.isNotBlank(productUpdate.getDescription())) {
            product.setDescription(productUpdate.getDescription());
        }

        product.setUpdatedAt(new Date());
        productRepository.save(product);
        log.info("Product is successfully updated: {}", product);
    }

    public void delete(Long productId) {
        if (!productRepository.exists(productId)) {
            throw new ProductNotFound(productId);
        }

        productRepository.delete(productId);
        log.info("Product is successfully removed: {}", productId);
    }

    public void updateDiscount(DiscountUpdate discountUpdate) {
        validate(discountUpdate);

        final Long productId = discountUpdate.getProductId();
        final Product product = productRepository.findOne(productId);
        validate(product, productId);

        Price price = product.getPrice();
        price.setDiscount(discountUpdate.getDiscount());
        price.setDiscountStart(discountUpdate.getStartDate());
        price.setDiscountEnd(discountUpdate.getEndDate());
        productRepository.save(product);

        log.info("Product's discount is successfully updated: {}", product);
    }

    /**
     * Returns the list of category products
     * <p/>
     * NOTE: Normally search engines like Solr, Elasticsearche, etc are used to get product list based on some search criteria
     */
    public List<ProductDTO> getCategoryProducts(Long categoryId) {
        Category category = categoryService.fetchCategory(categoryId);
        validate(category, categoryId);

        return category.getProducts().stream().map(ApiToDataConverter::convertToResult).collect(Collectors.toList());
    }

    /**
     * helper function for product create operation to set category
     */
    private void setCategory(Product product, ProductCreate productCreate) {
        final Long categoryId = productCreate.getCategoryId();
        if (categoryId != null) {
            Category category = categoryService.fetchCategory(productCreate.getCategoryId());
            validate(category, categoryId);
            product.setCategory(category);
        }
    }

    /**
     * helper function for product create operation to set price
     */
    private void setPrice(Product product, ProductCreate productCreate) {
        if (productCreate.getPrice() == null || !PriceUtil.isValidPrice(productCreate.getPrice())) {
            throw new InvalidPriceException(productCreate.getPrice());
        }

        final Price price = priceService.buildPrice(productCreate.getPrice(), productCreate.getCurrency());
        product.setPrice(price);
    }

    /**
     * helper function for product update operation to update category
     */
    private void updateCategory(Product product, ProductUpdate productUpdate) {
        if (productUpdate.getCategoryId() != null) {
            final Long categoryId = productUpdate.getCategoryId();
            final Category category = categoryService.fetchCategory(categoryId);
            validate(category, categoryId);
            product.setCategory(category);
        }
    }

    /**
     * helper function for product update operation to update price
     */
    private void updatePrice(Product product, ProductUpdate productUpdate) {
        final Price price = product.getPrice();
        if (productUpdate.getPrice() != null) {
            if(PriceUtil.isValidPrice(productUpdate.getPrice())) {
                BigDecimal convertedPrice = priceService.convertPrice(productUpdate.getPrice(), productUpdate.getCurrency());
                price.setPrice(convertedPrice);
            } else {
                throw new InvalidPriceException(productUpdate.getPrice());
            }
            price.setUpdatedAt(new Date());
        }
    }

    private void validate(DiscountUpdate discountUpdate) {
        if (discountUpdate.getDiscount() != null && !PriceUtil.isValidDiscount(discountUpdate.getDiscount())) {
            throw new IllegalArgumentException("Invalid price discount");
        }
        if (!PriceUtil.isValidDiscountDataRange(discountUpdate.getStartDate(), discountUpdate.getEndDate())) {
            throw new IllegalArgumentException("Invalid discount entity range");
        }
    }

    private void validate(Product product, Long productId) {
        if (product == null) {
            throw new ProductNotFound(productId);
        }
    }

    private void validate(Category category, Long categoryId) {
        if (category == null) {
            throw new CategoryNotFound(categoryId);
        }
    }
}
