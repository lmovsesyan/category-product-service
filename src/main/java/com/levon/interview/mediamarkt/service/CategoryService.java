package com.levon.interview.mediamarkt.service;

import com.levon.interview.mediamarkt.entity.Category;
import com.levon.interview.mediamarkt.exception.CategoryNotFound;
import com.levon.interview.mediamarkt.repository.CategoryRepository;
import com.levon.interview.mediamarkt.rest.dto.CategoryCreate;
import com.levon.interview.mediamarkt.rest.dto.CategoryDTO;
import com.levon.interview.mediamarkt.rest.dto.CategoryUpdate;
import com.levon.interview.mediamarkt.rest.util.ApiToDataConverter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Stack;
import java.util.stream.Collectors;

@Slf4j
@Service
@Transactional
public class CategoryService {

    private static final String CATEGORY_CACHE_NAME = "category.category";
    private static final String SUBCATEGORY_CACHE_NAME = "category.subcategory";

    private final CategoryRepository categoryRepository;

    @Autowired
    public CategoryService(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;;
    }

    /**
     * Returns the category DTO object for given category id
     */
    @Cacheable(value = CATEGORY_CACHE_NAME)
    public CategoryDTO getCategoryDTO(Long categoryId) {
        final Category category =  fetchCategory(categoryId);
        validate(category, categoryId);

        log.debug("Category fetched: {} ", category);

        CategoryDTO categoryDTO = ApiToDataConverter.convertToResult(category);
        categoryDTO.setFullPath(geCategoryFullPath(categoryId));

        log.debug("Category prepared: {}", categoryDTO);
        return categoryDTO;
    }

    @CacheEvict(cacheNames = SUBCATEGORY_CACHE_NAME, condition = "#categoryCreate.parentId != null", key = "#categoryCreate.parentId")
    public Long create(CategoryCreate categoryCreate) {
        Category category = new Category();

        setParentCategory(category, categoryCreate.getParentId());

        category.setName(categoryCreate.getName());
        category.setPath(categoryCreate.getPath());

        final Date now = new Date();
        category.setCreatedAt(now);
        category.setUpdatedAt(now);

        category =  categoryRepository.save(category);
        log.info("Category is successfully created: {}", category);
        return category.getId();
    }

    @Caching(evict = {
            @CacheEvict(value = CATEGORY_CACHE_NAME, key = "#categoryUpdate.id"),
            @CacheEvict(value = SUBCATEGORY_CACHE_NAME, allEntries = true)
    })
    public void update(CategoryUpdate categoryUpdate) {
        final Long categoryId = categoryUpdate.getId();
        final Category category = fetchCategory(categoryId);
        validate(category, categoryId);

        setParentCategory(category, categoryUpdate.getParentId());

        if (StringUtils.isNotBlank(categoryUpdate.getName())) {
            category.setName(categoryUpdate.getName());
        }
        if (StringUtils.isNotBlank(categoryUpdate.getPath())) {
            category.setPath(categoryUpdate.getPath());
        }

        category.setUpdatedAt(new Date());
        log.info("Category is successfully updated: {}", category);
        categoryRepository.save(category);
    }

    @Caching(evict = {
            @CacheEvict(value = CATEGORY_CACHE_NAME, key = "#id"),
            @CacheEvict(value = SUBCATEGORY_CACHE_NAME, allEntries = true)
    })
    public void delete(Long id) {
        if (!categoryRepository.exists(id)) {
            throw new CategoryNotFound(id);
        }

        final long productsCount = categoryRepository.countCategoryProducts(id);
        if (productsCount > 0) {
            log.warn("Can not delete category: category is used by {} products", productsCount);
            throw new IllegalStateException("Category is used by " + productsCount + " products");
        }

        final long categoryCounts = categoryRepository.countByParentId(id);
        if (categoryCounts > 0) {
            log.warn("Can not delete category: category is used by {} categories", categoryCounts);
            throw new IllegalStateException("Category is used by " + categoryCounts + " categories");
        }

        categoryRepository.delete(id);
        log.info("Category has been removed successfully: {}", id);
    }

    /**
     * Retrieves the top level categories (the category is top level when it does not have parent category)
     */
    public List<CategoryDTO> getTopCategories() {
        List<Category> topCategories = categoryRepository.findByParentId(null);
        log.info("{} top categories found", topCategories.size());
        //Note, that there is no need to calculate the full path for top level categories
        return topCategories.stream().map(ApiToDataConverter::convertToResult).collect(Collectors.toList());
    }

    /**
     * Get one level subcategories (the category A is subcategory of B, if B is parent of A)
     */
    @Cacheable(cacheNames = SUBCATEGORY_CACHE_NAME)
    public List<CategoryDTO> getImmediateSubcategories(Long categoryId) {
        Category category = fetchCategory(categoryId);
        validate(category, categoryId);

        List<Category> subcategories = category.getChildren();
        log.info("{} subcategories found", subcategories.size());

        return subcategories.stream().map(c -> {
            CategoryDTO categoryDTO = ApiToDataConverter.convertToResult(c);
            categoryDTO.setFullPath(geCategoryFullPath(c.getId()));
            return categoryDTO;
        }).collect(Collectors.toList());
    }

    /**
     * Builds and returns the category full path
     */
    String geCategoryFullPath(final long id) {
        Stack<String> pathStack = new Stack<>();

        Category category = fetchCategory(id);
        pathStack.add(category.getPath());

        while (category.getParent() != null) {
            category = fetchCategory(category.getParent().getId());
            pathStack.add(category.getPath());
        }

        StringBuilder stringBuilder = new StringBuilder();
        while (!pathStack.empty()) {
            stringBuilder.append("/").append(pathStack.pop());
        }
        return stringBuilder.toString();
    }

    /**
     * Returns the category DBO object directly from database
     */
    Category fetchCategory(Long categoryId) {
        return categoryRepository.findOne(categoryId);
    }

    /**
     * Helper function for create/update category operation to set parent category
     */
    private void setParentCategory(Category category, Long parentCategoryId) {
        if (parentCategoryId != null) {
            final Category parentCategory = fetchCategory(parentCategoryId);
            validate(parentCategory, parentCategoryId);

            category.setParent(parentCategory);
        }
    }

    private void validate(Category category, Long categoryId) {
        if (category == null) {
            throw new CategoryNotFound(categoryId);
        }
    }
}
