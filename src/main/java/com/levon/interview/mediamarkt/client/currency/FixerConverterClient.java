package com.levon.interview.mediamarkt.client.currency;

import com.levon.interview.mediamarkt.exception.CurrencyConvertException;
import com.levon.interview.mediamarkt.util.PriceUtil;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Map;

/**
 * Concrete implementation of {@link CurrencyExchangeClient} interface using
 * open REST API @see <a href="http://fixer.io">fixer.io</a>
 */
@Slf4j
public class FixerConverterClient implements CurrencyExchangeClient {

    private final String defaultCurrency;
    private final String apiUrlBase;

    private final RestTemplate restTemplate;

    public FixerConverterClient(RestTemplate restTemplate, String defaultCurrency, String apiUrlBase) {
        this.restTemplate = restTemplate;
        this.defaultCurrency = defaultCurrency;
        this.apiUrlBase = apiUrlBase;
    }

    @Override
    public BigDecimal convertAmount(BigDecimal amount, String currency) {
        try {
            validate(amount);
            validate(currency);

            if (StringUtils.equals(currency, defaultCurrency)) {
                log.warn("Requested to convert default currency. The same amount returned back");
                return amount;
            }

            log.info("Converting {} to {}. Amount is {}", currency, defaultCurrency, amount);

            final String url = buildLatestUrl(currency, defaultCurrency);
            final CurrencyRate currencyRate = restTemplate.getForObject(url, CurrencyRate.class);

            if (containsDefaultCurrency(currencyRate)) {
                return calculateConversion(amount, currency, currencyRate);
            } else {
                throw new CurrencyConvertException("Currency " + currency + " not supported");
            }
        } catch (RestClientException e) {
            log.error("Failed to convert {} to {}", currency, defaultCurrency, e);
            throw new CurrencyConvertException("The conversion service is unavailable");
        }
    }

    private BigDecimal calculateConversion(BigDecimal amount, String currency, CurrencyRate currencyRate) {
        final BigDecimal rate = new BigDecimal(currencyRate.getRates().get(defaultCurrency));
        log.debug("The currency rate from {} to is {}", currency, defaultCurrency, rate);
        final BigDecimal convertedAmount = PriceUtil.roundPrice(amount.multiply(rate));
        log.info("The amount {} converted to {} for {} currency", amount, convertedAmount, currency);
        return convertedAmount;
    }

    private String buildLatestUrl(String baseCurrency, String currency) {
        UriComponents uriComponents = UriComponentsBuilder.fromHttpUrl(apiUrlBase)
                .path("latest")
                .queryParam("base", baseCurrency)
                .queryParam("symbols", currency).build();
        return uriComponents.toString();
    }

    private boolean containsDefaultCurrency(CurrencyRate currencyRate) {
        return  currencyRate != null && currencyRate.getRates() != null && currencyRate.getRates().containsKey(defaultCurrency);
    }

    private void validate(BigDecimal amount) {
        if (!PriceUtil.isValidPrice(amount)) {
            log.warn("The amount is not valid: {}", amount);
            throw new IllegalArgumentException("Wrong amount is provided");
        }
    }

    private void validate(String currency) {
        if (StringUtils.isBlank(currency)) {
            final String msg = "The currency is blank";
            log.warn(msg);
            throw new IllegalArgumentException(msg);
        }
    }

    @Data
    static class CurrencyRate {

        private String base;
        private Date date;
        private Map<String, String> rates;
    }
}
