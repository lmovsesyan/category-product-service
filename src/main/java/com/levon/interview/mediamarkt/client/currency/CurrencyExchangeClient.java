package com.levon.interview.mediamarkt.client.currency;

import com.levon.interview.mediamarkt.exception.CurrencyConvertException;

import java.math.BigDecimal;

/**
 * Interface for currency exchange client
 */
public interface CurrencyExchangeClient {


    /**
     * Converts amount from given currency to default currency.
     * If the given currency is already default one then just returns back the given amount
     *
     * @throws CurrencyConvertException if conversion s not possible for any reason
     */
    BigDecimal convertAmount(BigDecimal amount, String currency) throws CurrencyConvertException;
}
