package com.levon.interview.mediamarkt;

import com.levon.interview.mediamarkt.client.currency.CurrencyExchangeClient;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles(Profile.TEST)
public abstract class AbstractTestConfig {

    @MockBean
    private CurrencyExchangeClient currencyExchangeClient;

    @Before
    public void before() {
        Mockito.when(currencyExchangeClient.convertAmount(Mockito.any(), Mockito.any()))
                .thenAnswer(invocation -> invocation.getArguments()[0]);
    }

}
