package com.levon.interview.mediamarkt.service;

import com.levon.interview.mediamarkt.AbstractTestConfig;
import com.levon.interview.mediamarkt.exception.CategoryNotFound;
import com.levon.interview.mediamarkt.rest.dto.CategoryCreate;
import com.levon.interview.mediamarkt.rest.dto.CategoryDTO;
import com.levon.interview.mediamarkt.rest.dto.CategoryUpdate;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;

import java.util.List;

public class CategoryServiceTest extends AbstractTestConfig {

    @Autowired
    private CategoryService categoryService;

    @Test
    public void hasNoParent() {
        CategoryDTO categoryDTO = categoryService.getCategoryDTO(1L);
        Assert.assertNotNull(categoryDTO);
        Assert.assertEquals(1, categoryDTO.getId());
        Assert.assertEquals("category1", categoryDTO.getName());
        Assert.assertEquals("path1", categoryDTO.getPath());
        Assert.assertNull(categoryDTO.getParentId());
    }

    @Test
    public void hasParent() {
        CategoryDTO categoryDTO = categoryService.getCategoryDTO(9L);
        Assert.assertNotNull(categoryDTO);
        Assert.assertEquals(9L, categoryDTO.getId());
        Assert.assertEquals("category9", categoryDTO.getName());
        Assert.assertEquals("path9", categoryDTO.getPath());
        Assert.assertEquals("/path2/path6/path9", categoryDTO.getFullPath());
        Assert.assertEquals(6, (long) categoryDTO.getParentId());
    }

    @Test
    public void createCategoryWithParent() {
        CategoryCreate categoryCreate = new CategoryCreate();
        categoryCreate.setName("Toys");
        categoryCreate.setPath("toys");
        categoryCreate.setParentId(4L);
        long categoryId = categoryService.create(categoryCreate);
        Assert.assertNotNull(categoryId);
        CategoryDTO categoryDTO = categoryService.getCategoryDTO(categoryId);
        Assert.assertNotNull(categoryDTO);
        Assert.assertEquals(categoryId, categoryDTO.getId());
        Assert.assertEquals("Toys", categoryDTO.getName());
        Assert.assertEquals("toys", categoryDTO.getPath());
        Assert.assertEquals(4, (long) categoryDTO.getParentId());
    }

    @Test
    public void createCategoryWithoutParent() {
        CategoryCreate categoryCreate = new CategoryCreate();
        categoryCreate.setName("Toys");
        categoryCreate.setPath("toys");
        long categoryId = categoryService.create(categoryCreate);
        Assert.assertNotNull(categoryId);
        CategoryDTO categoryDTO = categoryService.getCategoryDTO(categoryId);
        Assert.assertNotNull(categoryDTO);
        Assert.assertEquals(categoryId, categoryDTO.getId());
        Assert.assertEquals("Toys", categoryDTO.getName());
        Assert.assertEquals("toys", categoryDTO.getPath());
        Assert.assertNull(categoryDTO.getParentId());
    }

    @Test(expected = DataIntegrityViolationException.class)
    public void createCategoryMissName() {
        CategoryCreate categoryCreate = new CategoryCreate();
        categoryCreate.setPath("toys");
        categoryService.create(categoryCreate);
    }

    @Test
    public void update() {
        final long CATEGORY_UPDATE = 8;

        CategoryDTO categoryDTO = categoryService.getCategoryDTO(CATEGORY_UPDATE);
        Assert.assertNotNull(categoryDTO);
        Assert.assertEquals(CATEGORY_UPDATE, categoryDTO.getId());
        Assert.assertEquals("category" + CATEGORY_UPDATE, categoryDTO.getName());
        Assert.assertEquals("path" + CATEGORY_UPDATE, categoryDTO.getPath());
        Assert.assertEquals(2, (long) categoryDTO.getParentId());

        CategoryUpdate categoryUpdate = new CategoryUpdate();
        categoryUpdate.setId(CATEGORY_UPDATE);
        categoryUpdate.setName("updatedName");
        categoryService.update(categoryUpdate);

        categoryDTO = categoryService.getCategoryDTO(CATEGORY_UPDATE);
        Assert.assertNotNull(categoryDTO);
        Assert.assertEquals(CATEGORY_UPDATE, categoryDTO.getId());
        Assert.assertEquals("updatedName", categoryDTO.getName());
        Assert.assertEquals("path" + CATEGORY_UPDATE, categoryDTO.getPath());
        Assert.assertEquals(2, (long) categoryDTO.getParentId());
    }

    @Test(expected = CategoryNotFound.class)
    public void deleteNotFound() {
        categoryService.delete(100000L);
    }

    @Test(expected = IllegalStateException.class)
    public void deleteUsedByProduct() {
        categoryService.delete(1L);
    }

    @Test(expected = IllegalStateException.class)
    public void deleteUsedByCategory() {
        categoryService.delete(14L);
    }

    @Test
    public void deleteSuccess() {
        categoryService.delete(13L);
    }

    @Test
    public void deepCategoryPath() {
        String path = categoryService.geCategoryFullPath(5L);
        Assert.assertNotNull(path);
        Assert.assertEquals("/path1/path3/path5", path);
    }

    @Test
    public void topCategoryPath() {
        String path = categoryService.geCategoryFullPath(1L);
        Assert.assertNotNull(path);
        Assert.assertEquals("/path1", path);
    }

    @Test
    public void categoryChildren() {
        List<CategoryDTO> categories = categoryService.getImmediateSubcategories(1L);
        Assert.assertNotNull(categories);
        Assert.assertEquals(2, categories.size());
    }

    @Test
    public void topCategories() {
        List<CategoryDTO> topCategories = categoryService.getTopCategories();
        Assert.assertFalse(topCategories.isEmpty());
        topCategories.forEach(c -> Assert.assertNull(c.getParentId()));
    }
}
