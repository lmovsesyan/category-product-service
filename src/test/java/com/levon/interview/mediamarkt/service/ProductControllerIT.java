package com.levon.interview.mediamarkt.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.levon.interview.mediamarkt.Profile;
import com.levon.interview.mediamarkt.rest.dto.DiscountUpdate;
import com.levon.interview.mediamarkt.rest.dto.ProductCreate;
import com.levon.interview.mediamarkt.rest.dto.ProductUpdate;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.math.BigDecimal;

import static org.hamcrest.Matchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles(Profile.TEST)
public class ProductControllerIT {

    private static final long PRODUCT_UPDATE = 12;


    @Autowired
    private MockMvc mockMvc;

    @Test
    public void createProduct() throws Exception {
        ProductCreate productCreate = new ProductCreate();
        productCreate.setName("integration");
        productCreate.setDescription("I am an integration product");
        productCreate.setCategoryId(1L);
        productCreate.setCurrency("USD");
        productCreate.setPrice(new BigDecimal(100));
        String json = new ObjectMapper().writeValueAsString(productCreate);
        mockMvc.perform(post("/api/v1/product/").content(json).contentType(MediaType.APPLICATION_JSON))
                .andDo(print()).andExpect(status().isOk());

    }

    @Test
    public void getProduct() throws Exception {
        mockMvc.perform(get("/api/v1/product/1")).andDo(print()).andExpect(status().isOk())
                .andExpect(content().string(containsString("product1")));
    }

    @Test
    public void updateProduct() throws Exception {
        ProductUpdate productUpdate = new ProductUpdate();
        productUpdate.setId(PRODUCT_UPDATE);
        productUpdate.setName("This is my new name");
        productUpdate.setDescription("Hi there, I am a description. Check me!");
        productUpdate.setPrice(new BigDecimal(1000));

        String json = new ObjectMapper().writeValueAsString(productUpdate);

        mockMvc.perform(put("/api/v1/product/").content(json).contentType(MediaType.APPLICATION_JSON))
                .andDo(print()).andExpect(status().isOk())
                .andExpect(content().string(containsString("true")));

        mockMvc.perform(get("/api/v1/product/" + PRODUCT_UPDATE)).andDo(print()).andExpect(status().isOk())
                .andExpect(content().string(containsString("This is my new name")))
                .andExpect(content().string(containsString("Hi there, I am a description. Check me!")))
                .andExpect(content().string(containsString("1000")));
    }

    @Test
    public void updateDiscount() throws Exception {
        DiscountUpdate productUpdate = new DiscountUpdate(PRODUCT_UPDATE);
        productUpdate.setDiscount(10);

        String json = new ObjectMapper().writeValueAsString(productUpdate);

        mockMvc.perform(put("/api/v1/product/discount").content(json).contentType(MediaType.APPLICATION_JSON))
                .andDo(print()).andExpect(status().isOk())
                .andExpect(content().string(containsString("true")));

        mockMvc.perform(get("/api/v1/product/" + PRODUCT_UPDATE)).andDo(print()).andExpect(status().isOk())
                .andExpect(content().string(containsString("\"discount\":10")));
    }
}
