package com.levon.interview.mediamarkt.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.levon.interview.mediamarkt.Profile;
import com.levon.interview.mediamarkt.rest.dto.CategoryCreate;
import com.levon.interview.mediamarkt.rest.dto.CategoryUpdate;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles(Profile.TEST)
public class CategoryControllerIT {

    private static final long CATEGORY_UPDATE = 12;

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void createCategory() throws Exception {
        CategoryCreate categoryCreate = new CategoryCreate();
        categoryCreate.setName("integration");
        categoryCreate.setParentId(1L);
        categoryCreate.setPath("integration-path");
        String json = new ObjectMapper().writeValueAsString(categoryCreate);
        mockMvc.perform(post("/api/v1/category").content(json).contentType(MediaType.APPLICATION_JSON))
                .andDo(print()).andExpect(status().isOk());

    }

    @Test
    public void getCategory() throws Exception {
        mockMvc.perform(get("/api/v1/category/1")).andDo(print()).andExpect(status().isOk())
                .andExpect(content().string(containsString("category1")));
    }

    @Test
    public void updateCategory() throws Exception {
        CategoryUpdate productUpdate = new CategoryUpdate();
        productUpdate.setId(CATEGORY_UPDATE);
        productUpdate.setName("Servus! Ich heiße category");
        productUpdate.setParentId(1L);

        String json = new ObjectMapper().writeValueAsString(productUpdate);

        mockMvc.perform(put("/api/v1/category/").content(json).contentType(MediaType.APPLICATION_JSON))
                .andDo(print()).andExpect(status().isOk())
                .andExpect(content().string(containsString("true")));

        mockMvc.perform(get("/api/v1/category/" + CATEGORY_UPDATE)).andDo(print()).andExpect(status().isOk())
                .andExpect(content().string(containsString("Servus! Ich heiße category")))
                .andExpect(content().string(containsString("\"parentId\":1")))
                .andExpect(content().string(containsString("/path1/path"+ CATEGORY_UPDATE)));
    }

    @Test
    public void topCategories() throws Exception {
        mockMvc.perform(get("/api/v1/category/top")).andDo(print()).andExpect(status().isOk());
    }

    @Test
    public void subcategories() throws Exception {
        mockMvc.perform(get("/api/v1/category/subcategories/1")).andDo(print()).andExpect(status().isOk());
    }
}
