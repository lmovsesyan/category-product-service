package com.levon.interview.mediamarkt.service;

import com.levon.interview.mediamarkt.AbstractTestConfig;
import com.levon.interview.mediamarkt.exception.ProductNotFound;
import com.levon.interview.mediamarkt.rest.dto.DiscountUpdate;
import com.levon.interview.mediamarkt.rest.dto.ProductCreate;
import com.levon.interview.mediamarkt.rest.dto.ProductDTO;
import com.levon.interview.mediamarkt.rest.dto.ProductUpdate;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class ProductServiceTest extends AbstractTestConfig {

    private final static long PRODUCT_GET = 3;
    private final static long PRODUCT_DELETE = 11;
    private final static long PRODUCT_UPDATE = 5;
    private final static long PRODUCT_VALID_DISCOUNT = 1;
    private final static long PRODUCT_EXPIRED_DISCOUNT = 9;

    @Autowired
    private ProductService productService;

    @Test
    public void getProduct() {
        ProductDTO productDTO = productService.getProduct(PRODUCT_GET);
        Assert.assertNotNull(productDTO);
        Assert.assertNotNull(productDTO.getCategory());
        Assert.assertEquals(8, productDTO.getCategory().getId());
        Assert.assertEquals("product" + PRODUCT_GET, productDTO.getName());
        Assert.assertEquals("100.00", productDTO.getPrice().toString());

        Assert.assertNull(productDTO.getDescription());
        Assert.assertNull(productDTO.getDiscount());
    }

    @Test
    public void getDiscountProduct() {
        ProductDTO productDTO = productService.getProduct(PRODUCT_VALID_DISCOUNT);
        Assert.assertNotNull(productDTO);
        Assert.assertNotNull(productDTO.getCategory());
        Assert.assertEquals(4, (long) productDTO.getCategory().getId());
        Assert.assertEquals("product" + PRODUCT_VALID_DISCOUNT, productDTO.getName());
        Assert.assertEquals("45.50", productDTO.getPrice().toString());
        Assert.assertEquals("50.55", productDTO.getPreviousPrice().toString());

        Assert.assertNull(productDTO.getDescription());
        Assert.assertEquals(10, (int) productDTO.getDiscount());
    }

    @Test
    public void getExpiredDiscountProduct() {
        ProductDTO productDTO = productService.getProduct(PRODUCT_EXPIRED_DISCOUNT);
        Assert.assertNotNull(productDTO);
        Assert.assertNotNull(productDTO.getCategory());
        Assert.assertEquals(9, (long) productDTO.getCategory().getId());
        Assert.assertEquals("product" + PRODUCT_EXPIRED_DISCOUNT, productDTO.getName());
        Assert.assertEquals("25.27", productDTO.getPrice().toString());

        Assert.assertNull(productDTO.getDescription());
        Assert.assertNull(productDTO.getDiscount());
    }

    @Test
    public void createSimpleProduct() {
        ProductCreate productCreate = new ProductCreate();
        productCreate.setName("Orange Juice");
        productCreate.setPrice(BigDecimal.valueOf(3.5));
        Long productId = productService.create(productCreate);
        Assert.assertNotNull(productId);

        ProductDTO productDTO = productService.getProduct(productId);
        Assert.assertNotNull(productDTO);
        Assert.assertNull(productDTO.getCategory());
        Assert.assertNull(productDTO.getDescription());
        Assert.assertNull(productDTO.getDiscount());
        Assert.assertNull(productDTO.getPreviousPrice());
        Assert.assertEquals("3.50", productDTO.getPrice().toString());
        Assert.assertEquals("Orange Juice", productDTO.getName());
    }

    @Test
    public void createFullProduct() {
        ProductCreate productCreate = new ProductCreate();
        productCreate.setName("Orange Juice");
        productCreate.setDescription("Orange juice contains many vitamins");
        productCreate.setPrice(BigDecimal.valueOf(100));

        Long productId = productService.create(productCreate);

        DiscountUpdate discountUpdate = new DiscountUpdate(productId);
        discountUpdate.setDiscount(10);
        discountUpdate.setStartDate(yesterday());
        discountUpdate.setEndDate(tomorrow());
        productService.updateDiscount(discountUpdate);

        Assert.assertNotNull(productId);

        ProductDTO productDTO = productService.getProduct(productId);
        Assert.assertNotNull(productDTO);
        Assert.assertNull(productDTO.getCategory());
        Assert.assertEquals(10, (int) productDTO.getDiscount());
        Assert.assertEquals("100.00", productDTO.getPreviousPrice().toString());
        Assert.assertEquals("90.00", productDTO.getPrice().toString());
        Assert.assertEquals("Orange Juice", productDTO.getName());
        Assert.assertEquals("Orange juice contains many vitamins", productDTO.getDescription());
    }

    @Test(expected = IllegalArgumentException.class)
    public void createInvalidDiscountDateRangeProduct() {
        ProductCreate productCreate = new ProductCreate();
        productCreate.setName("Orange Juice");
        productCreate.setDescription("Orange juice contains many vitamins");
        productCreate.setPrice(BigDecimal.valueOf(100));
        Long productId = productService.create(productCreate);

        DiscountUpdate discountUpdate = new DiscountUpdate(productId);
        discountUpdate.setDiscount(10);
        discountUpdate.setEndDate(yesterday());
        discountUpdate.setStartDate(tomorrow());
        productService.updateDiscount(discountUpdate);
        Assert.assertNotNull(productId);
    }

    @Test(expected = IllegalArgumentException.class)
    public void createInvalidDiscountProduct() {
        ProductCreate productCreate = new ProductCreate();
        productCreate.setName("Orange Juice");
        productCreate.setDescription("Orange juice contains many vitamins");
        productCreate.setPrice(BigDecimal.valueOf(100));
        Long productId = productService.create(productCreate);
        Assert.assertNotNull(productId);

        DiscountUpdate discountUpdate = new DiscountUpdate(productId);
        discountUpdate.setDiscount(120);
        discountUpdate.setStartDate(yesterday());
        discountUpdate.setEndDate(tomorrow());
        productService.updateDiscount(discountUpdate);
        Assert.assertNotNull(productId);
    }

    @Test
    public void update() {
        final String name = "Tic Tac Toe";
        final String description = "Tic Tac Toe is my first game";

        ProductUpdate productUpdate = new ProductUpdate();
        productUpdate.setId(PRODUCT_UPDATE);
        productUpdate.setName(name);
        productUpdate.setDescription(description);
        productUpdate.setCategoryId(1L);
        productService.update(productUpdate);
        ProductDTO productDTO = productService.getProduct(PRODUCT_UPDATE);
        Assert.assertNotNull(productDTO);
        Assert.assertEquals(name, productDTO.getName());
        Assert.assertEquals(description, productDTO.getDescription());
        Assert.assertNotNull(productDTO.getCategory());
        Assert.assertEquals(1, (long) productDTO.getCategory().getId());
        productUpdate.setId(PRODUCT_UPDATE);
        productUpdate.setName(null);
        productUpdate.setDescription(null);
        productUpdate.setCategoryId(6L);
        productService.update(productUpdate);
        productDTO = productService.getProduct(PRODUCT_UPDATE);
        Assert.assertNotNull(productDTO);
        Assert.assertEquals(name, productDTO.getName());
        Assert.assertEquals(description, productDTO.getDescription());
        Assert.assertNotNull(productDTO.getCategory());
        Assert.assertEquals(6, (long) productDTO.getCategory().getId());
    }

    @Test
    public void updatePrice() {
        final String name = "Chess";

        ProductUpdate productUpdate = new ProductUpdate();
        productUpdate.setId(PRODUCT_UPDATE);
        productUpdate.setPrice(BigDecimal.valueOf(27.11));
        productService.update(productUpdate);
        ProductDTO productDTO = productService.getProduct(PRODUCT_UPDATE);
        Assert.assertNotNull(productDTO);
        Assert.assertEquals("27.11", productDTO.getPrice().toString());
        productUpdate.setId(PRODUCT_UPDATE);
        productUpdate.setName(name);
        productService.update(productUpdate);
        productDTO = productService.getProduct(PRODUCT_UPDATE);
        Assert.assertNotNull(productDTO);
        Assert.assertEquals("27.11", productDTO.getPrice().toString());
        Assert.assertEquals(name, productDTO.getName());
    }

    @Test
    public void updatePriceDiscount() {
        DiscountUpdate discountUpdate = new DiscountUpdate(PRODUCT_UPDATE);
        discountUpdate.setDiscount(50);
        discountUpdate.setStartDate(yesterday());
        discountUpdate.setEndDate(tomorrow());
        productService.updateDiscount(discountUpdate);
        ProductDTO productDTO = productService.getProduct(PRODUCT_UPDATE);
        Assert.assertNotNull(productDTO);
        Assert.assertEquals(50, (int) productDTO.getDiscount());

        discountUpdate = new DiscountUpdate(PRODUCT_UPDATE);
        productService.updateDiscount(discountUpdate);
        productDTO = productService.getProduct(PRODUCT_UPDATE);
        Assert.assertNotNull(productDTO);
        Assert.assertNull(productDTO.getDiscount());
    }

    @Test(expected = ProductNotFound.class)
    public void delete() {
        ProductDTO productDTO = productService.getProduct(PRODUCT_DELETE);
        Assert.assertNotNull(productDTO);
        productService.delete(PRODUCT_DELETE);
        productService.getProduct(PRODUCT_DELETE);
    }

    @Test
    public void categoryProducts() {
        List<ProductDTO> categories = productService.getCategoryProducts(1L);
        Assert.assertNotNull(categories);
        Assert.assertTrue(!categories.isEmpty());
    }

    private Date yesterday() {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_MONTH, -1);
        return calendar.getTime();
    }

    private Date tomorrow() {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_MONTH, 1);
        return calendar.getTime();
    }
}
