package com.levon.interview.mediamarkt.client.currency;


import com.levon.interview.mediamarkt.exception.CurrencyConvertException;
import com.levon.interview.mediamarkt.util.PriceUtil;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import com.levon.interview.mediamarkt.client.currency.FixerConverterClient.CurrencyRate;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.eq;

public class FixerConverterClientTest {
    
    private final String USD = "USD";
    private final String AMD = "AMD";

    private final String defaultCurrency = "EUR";
    private final String apiBaseUrl = "http://fake.url";
    private final RestTemplate restTemplate = mock(RestTemplate.class);

    private CurrencyExchangeClient client = new FixerConverterClient(restTemplate, defaultCurrency, apiBaseUrl);

    @Test
    public void convertUsdSuccess() {
        CurrencyRate currencyRate = buildCurrencyRate(USD, "0.8");
        when(restTemplate.getForObject(eq(apiBaseUrl + "/latest?base=USD&symbols=EUR"),
                eq(FixerConverterClient.CurrencyRate.class))).thenReturn(currencyRate);

        BigDecimal convertedAmount = client.convertAmount(BigDecimal.valueOf(100), USD);
        Assert.assertEquals(PriceUtil.roundPrice(BigDecimal.valueOf(80)), convertedAmount);
    }

    @Test
    public void convertAmdSuccess() {
        CurrencyRate currencyRate = buildCurrencyRate(AMD, "502");
        when(restTemplate.getForObject(eq(apiBaseUrl + "/latest?base=AMD&symbols=EUR"),
                eq(FixerConverterClient.CurrencyRate.class))).thenReturn(currencyRate);

        BigDecimal convertedAmount = client.convertAmount(BigDecimal.valueOf(1000), AMD);
        Assert.assertEquals(PriceUtil.roundPrice(BigDecimal.valueOf(502000)), convertedAmount);
    }

    @Test(expected = IllegalArgumentException.class)
    public void convertIllegalAmount() {
        client.convertAmount(BigDecimal.valueOf(-10), USD);
    }

    @Test(expected = IllegalArgumentException.class)
    public void convertIllegalCurrency() {
        client.convertAmount(BigDecimal.valueOf(10), "   ");
    }

    @Test(expected = CurrencyConvertException.class)
    public void convertNotSupported() {
        CurrencyRate currencyRate = buildCurrencyRate(USD, "0.8");
        when(restTemplate.getForObject(eq(apiBaseUrl + "/latest?base=USD&symbols=EUR"),
                eq(FixerConverterClient.CurrencyRate.class))).thenReturn(currencyRate);

        client.convertAmount(BigDecimal.valueOf(90), AMD);
    }

    @Test
    public void convertSameAmount() {
        BigDecimal convertedAmount = client.convertAmount(BigDecimal.valueOf(100), "EUR");
        Assert.assertEquals(new BigDecimal(100),  convertedAmount);
    }

    @Test(expected = CurrencyConvertException.class)
    public void convertServerUnavailable() {
        when(restTemplate.getForObject(eq(apiBaseUrl + "/latest?base=USD&symbols=EUR"),
                eq(FixerConverterClient.CurrencyRate.class))).thenThrow(new RestClientException("Server is unavailable"));

        client.convertAmount(BigDecimal.valueOf(90), AMD);
    }

    private CurrencyRate buildCurrencyRate(String currency, String rate) {
        CurrencyRate currencyRate = new CurrencyRate();
        currencyRate.setBase(currency);
        currencyRate.setDate(new Date());
        Map<String, String> rates = new HashMap<>();
        rates.put(defaultCurrency, rate);
        currencyRate.setRates(rates);
        return currencyRate;
    }

}
